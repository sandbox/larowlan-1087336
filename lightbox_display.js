// $Id$
/*
 * @file lightbox_display.js
 * Provides the lightbox_display auto discovery of links with class lightbox_display
 * @copyright Copyright(c) 2011 Lee Rowlands
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Lee Rowlands contact at rowlandsgroup dot com
 * 
 */
Drupal.behaviors.lightbox_display = function(context) {
  //append the lightbox to the path
  $('a.lightbox_display:not(.ld-processed)').addClass('ld-processed').each(function(){
    var href = $(this).attr('href');
    if (href.indexOf('?') != -1) {
      var parts = href.split('?');
      href = parts[0] + '?' + parts[1] + '&lightbox=1';
    }
    else {
      //we append the lightbox path to the url so the js version can use a different page tpl
      href += '?lightbox=1'; 
    }
    $(this).attr('href', href);
  })
}
