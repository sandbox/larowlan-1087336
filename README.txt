//$Id$
Use this module to provide yourself with some theming flexibility when using lightbox modules to display nodes including
*Colorbox
*Thickbox
*Lightbox2

This module registers a menu callback at node/xx/lightbox where xx is the node id.
So node/3 would be the full view of the node whilst node/3/lightbox will be the lightbox view.
It is up to you to change your node links to point to node/xx/lightbox where and configure your lightbox module appropriately.
This is most likely achieved through other theming overrides. One way to do this is to
add the class lightbox_display to any link, the module scans the page for these and automatically
appends the required path argument.

** Other than node links **
The module also works with 'other than node' links but if you are serving forms in those
pages then you need to handle the action of the form TO REMOVE THE lightbox ARGUMENT
from the path, otherwise your form will mistakenly redirect to the lightbox version
on error. This is achieved by hook_form_alter.
For other than node links, use the class lightbox_display to append the lightbox argument (via this module's auto-discovery js)
to your path and then you're away. The module ships with a page-lightbox to handle those instances
and this provides a basic output, as well as degrading gracefully for those users without js.
You can provide specific overrides for the standard template by copying page-lightbox.tpl.php
from this module into your theme folder and renaming as required eg page-user-lightbox.tpl.php.

----------------------------------
So what do I get with this module?
----------------------------------

** CCK Formatters **

Now you have a lightbox callback you can alter the CCK field display formats - visit /admin/content/node-type/xxx/display/lightbox_display
to choose new CCK formats for your fields when shown at the lightbox path. Substitue xxx for your content type.

** Node templates **

The module registers new template suggestions for theming your node as follows
node--lightbox.tpl.php
node-xxx--lightbox.tpl.php where xxx is your content type's machine name
You can copy the file node--lightbox.tpl.php from this module's folder to your theme folder and edit it to suit your desired lightbox output

** Page templates **

If you are using your lightbox popup with an iframe (eg lightbox2's lightframe mode, colorbox also supports iframes) then the module provides
page-node-lightbox.tpl.php, copy it to your theme folder and customise to suit. The version supplied with the module strips out the header, sidebars, logo, tabs etc
as you normally don't want these in your iframe lightbox.
